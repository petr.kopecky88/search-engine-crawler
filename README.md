# Search Engines Crawler

Simple bot used for crawling search engines to get results related to a specified site name. <strong>Only works with non-spa sites.</strong>

## Supported search engines

* seznam.cz
* google.com

## Additional information

Default output is printed to stdout. Run with `-h` to see other options.

## Prerequisites

### Mandatory

* Python 3.7
* Linux (with Bash shell)
* Libraries (Ubuntu)
  * libxml2-dev
  * libxslt-dev
  * build-essential

### Optional

* Docker

## Run

### Docker

1. Run script with sitename:

    ```bash
    bash run.sh example.com
    ```
### OS

1. Install dependencies:

    ```bash
    sudo apt-get install libxml2-dev libxslt-dev build-essential
    ```

2. Install Python dependencies:

    ```bash
    pip install -r requirements.txt
    ```

3. Run the script

    ```bash
    python crawler.py <SITENAME>
    ```

4. _Optional: Run help for more information:_

    ```bash
    python crawler.py -h
    ```
