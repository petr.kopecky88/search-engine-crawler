from abc import ABCMeta, abstractmethod
import urllib.parse
import urllib.request
import lxml.html
import lxml.etree


class AbstractEngine(metaclass=ABCMeta):
    """Base class for all engines."""

    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0"
    }

    def __init__(self, base_url, params):
        self.base_url = base_url
        self.params = params

    def process(self) -> set:
        # Build search URL.
        search_url = list(urllib.parse.urlparse(self.base_url))
        search_url[2] = self.params['url_path']
        search_url[4] = urllib.parse.urlencode(self.params["query_params"])
        search_url = urllib.parse.urlunparse(search_url)

        req = urllib.request.Request(search_url, headers=self.headers, method="GET")

        all_urls = set()
        urls, next_page = self._fetch_urls(req)
        all_urls.update(urls)

        # Fetch all results from individual next pages.
        while len(next_page) > 0:
            next_page = next_page[0]
            if "href" not in next_page.attrib:
                break

            search_url = self.base_url
            search_url += next_page.attrib["href"]

            req = urllib.request.Request(search_url, headers=self.headers, method="GET")

            urls, next_page = self._fetch_urls(req)
            all_urls.update(urls)

        return all_urls

    @property
    @abstractmethod
    def name(self) -> str:
        """Name of the engine.

        Returns:
            str: Name of the search engine.
        """
        raise NotImplementedError("Must define 'name' to use this class,")

    @property
    @abstractmethod
    def search_patterns(self) -> list:
        """CSS style search patterns that contains links.

        Returns:
            list (str): List of search patterns in CSS selector format.
        """
        raise NotImplementedError("Must define 'search_patterns' to use this class,")

    @property
    @abstractmethod
    def next_page_selector(self) -> str:
        """CSS style selector for next page link.

        Returns:
            str: CSS selector.
        """

    def _fetch_urls(self, req) -> tuple:
        """Fetch URLs for actual page.

        Arguments:
            req (urllib.request.Request): Request to process.

        Returns:
            tuple: (urls [set], next page element [HTMLElement])
        """

        urls = set()

        with urllib.request.urlopen(req) as res:
            data = res.read()
            doc = lxml.html.fromstring(str(data))

            # Find all patterns.
            for pattern in self.search_patterns:
                elements = doc.cssselect(pattern)

                for el in elements:
                    # Get URL without scheme and host.
                    url = urllib.parse.urlparse(el.attrib["href"])
                    urls.add(url.path)

            next_page = doc.cssselect(self.next_page_selector)

        return urls, next_page


class SeznamEngine(AbstractEngine):
    """Class to parse output from seznam.cz search engine."""

    def __init__(self, site_name) -> None:
        base_url = "https://search.seznam.cz"
        params = {
            "url_path": "/",
            "query_params": {
                "q": "site:" + site_name,
                "count": 10,
                # Default value. This will increment when crawling pages.
                "from": 0,
            }
        }

        super().__init__(base_url, params)

    @property
    def name(self) -> str:
        return "seznam.cz"

    @property
    def search_patterns(self) -> list:
        return ["a.Result-url-link"]

    @property
    def next_page_selector(self):
        return "ul.Paging>li.Paging-item--next>a"


class GoogleEngine(AbstractEngine):
    """Class to parse output from google.com search engine."""

    def __init__(self, site_name) -> None:
        base_url = "https://www.google.com"
        params = {
            "url_path": "/search",
            "query_params": {
                "q": "site:" + site_name,
                "start": 0
            }
        }

        super().__init__(base_url, params)

    @property
    def name(self) -> str:
        return "google.com"

    @property
    def search_patterns(self) -> list:
        return ["div.rc>div.r>a"]

    @property
    def next_page_selector(self) -> str:
        return "#pnnext.pn"


class BingEngine(AbstractEngine):
    """Class to parse output from bing.com search engine."""

    def __init__(self, site_name) -> None:
        base_url = "https://www.bing.com"
        params = {
            "url_path": "/search",
            "query_params": {
                "q": "site:" + site_name,
                "first": 1
            }
        }

        super().__init__(base_url, params)

    @property
    def name(self) -> str:
        return "bing.com"

    @property
    def search_patterns(self) -> list:
        return ["#b_results>li.b_algo>h2>a"]

    @property
    def next_page_selector(self) -> str:
        return "#b_results>li.b_pag>nav>ul>li>a.sb_pagN.sb_pagN_bp.b_widePag.sb_bp"
