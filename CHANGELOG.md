# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0](https://gitlab.com/petr.kopecky88/search-engine-crawler/compare/0.1.0...0.2.0) - 2018-12-27

### Updated

- Allow output to be printed to stdout. 
- Make parsing process easier.
- Save output into temp dir.

### Added

- Add BingEngine.
- Add GoogleEngine.

## [0.1.0](https://gitlab.com/petr.kopecky88/search-engine-crawler/compare/d8ad047f6c91c2db8f393310bd8b92c73d0bd30e...0.1.0) - 2018-12-27

### Added

- Add MIT license.
- Run scripts.
- Add SeznamEngine.
- Add AbstractEngine.
- README and CHANGELOG.