import argparse
import pathlib
from tempfile import gettempdir
from engines import *
from os import linesep


parser = argparse.ArgumentParser(description="Find all links for a given site on different sites and print them to stdout.")
parser.add_argument("site", type=str, nargs="?", help="site to search for")
parser.add_argument("-f", "--output-to-file", help="output to file in 'tmp' folder", action="store_true")

args = parser.parse_args()

if __name__ == '__main__':
    if not args.site:
        parser.error("Please specify site.")
    # Get all engines to parse.
    engines = AbstractEngine.__subclasses__()
    all_urls = set()

    for engine in engines:
        site_name = args.site

        e = engine(site_name)
        urls = e.process()

        if args.output_to_file:
            # Create temporary dir.
            save_dir = "{}/{}/{}".format(gettempdir(), "search-engine-crawler", e.name)
            pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)

            save_path = pathlib.Path("{}/{}_urls.txt".format(save_dir, site_name))
            with save_path.open('w') as f:
                for url in sorted(urls):
                    f.write(url + "\n")
        else:
            all_urls.update(urls)

    if not args.output_to_file:
        print(*sorted(all_urls), sep=linesep)
