#!/bin/bash

# Provisioning.
cmd="apk add build-base libxml2-dev libxslt-dev && pip install --no-cache-dir -r requirements.txt && python crawler.py $1"

docker run --rm -it --name search-engines-crawler -v $(pwd):/usr/src/app -w /usr/src/app python:3.7-alpine ash -c "$cmd"